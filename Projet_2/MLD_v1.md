**MLD : Gestion de parkings**

**Client** (\#id : integer, nom : string, prenom : string)

**Occasionnel** (\#id\_occasionnel \=\>Client)

**Abonne** (\#id\_abonne \=\>Client)

**Compte\_Fidelite** (\#id\_fidelite : integer, date\_creation : date, points : integer, niveau : {‘bronze’|’argent’|’or’}, id\_abonne \=\>Client)   
**avec** id\_abonne KEY

**Carte\_Abonnement** (\#id : integer, date\_de\_debut : date, date\_de\_fin : date, compte\_fidelite=\> Compte\_Fidelite, id\_abonne \=\>Client, num\_transaction : integer, date\_transaction : date, moyen\_paiement : {‘carte’|’guichet’}, zone \=\>Zone)   
**avec** id\_abonne NOT NULL **et** zone NOT NULL **et** num\_transaction KEY

**Zone** (\#id\_zone : integer, nom : string, prenom : string, tarif\_horaire : money, tarif\_abo : money)

**Parking** (\#id\_parking : integer, nom : string, adresse : string, nombre\_places : integer, zone\_parking \=\>Zone)   
	**avec** zone\_parking NOT NULL

**Place** (\#id\_place : integer, numéro : string, statut : {‘libre’|’occupee’|’reservee’}, type\_de\_véhicule : {‘deux-roues’|’camion’|’voiture’}, type\_zone : {‘couverte’|’plein\_air’}, parking \=\>Parking)

**Véhicule** (\#immatriculation : string, type\_de\_véhicule : {‘deux-roues’|’camion’|’voiture’}, place\_véhicule \=\>Place, ticket\_vehicule \=\>Ticket, id\_client \=\>Client)   
**avec** place\_véhicule KEY **et** ticket\_vehicule KEY **et** id\_client NOT NULL

**Ticket** (\#id\_ticket : integer, debut:  time, fin : time, date : date,  num\_transaction:integer, date\_transaction : date, moyen\_paiement : {‘carte’|’guichet’}, zone \=\>Zone)   
**avec** zone NOT NULL **et** num\_transaction KEY

**Réservation** (\#id\_client \=\>Client, \#place\_reservee \=\>Place, \#id\_reservation : integer, date\_debut : date, date\_fin : date)

**Tous les attributs sont non nuls.**

**Contraintes complexes :** 

- Projection(Abonne, id\_abonne)=Projection(Carte\_Abonnement, id\_abonnement)  
- Projection(Client, id)=Projection(Vehicule, id\_client)  
- Intersection(Projection(Ticket, num\_transaction), Projection(Carte\_Abonnement, num\_transaction)={}

**Justifications :**

Héritages

- un héritage par référence pour Client, car on a une classe mère abstraite et associée en relation sortante N:M, et que l’héritage est non complet   
- un héritage par les classes filles pour Transaction, car la classe mère est abstraite et non associée, et l’héritage est non complet 

Relations 1–1:

- Pour la relation Ticket 1–1 Véhicule on a préféré une référence dans Véhicule vers Ticket qui semblait plus logique  
- Pour la relation Place 1–0..1 Véhicule on a préféré une référence dans Véhicule vers Place qui semblait plus logique   
- Pour la relation Compte\_Fidelite 0..1–1 Abonne on a préféré une référence dans Compte\_Fidelite vers Abonne qui semblait plus logique

Agrégation et composition:

- Agrégation Carte\_Abonnement et Carte\_Fidelite car la carte abonnement peut faire partie d’un compte de fidélité mais existe indépendamment du compte de fidélité  
- Composition Parking/Place car une place n’existe pas sans parking


# Note de clarification 

## Gestion des Parkings

## 1. Clients

__La classe Client possèdent les attributs:__

>- id_client : un client est identifié par une clé artificielle id car on peut avoir deux clients avec le même nom et prénom  
>- nom : nom du client  
>- prenom : prenom du client 
> 
 > Les classes héritées, Abonne et Occasionnel, hériteront des attributs de Client qu’on a considéré comme une classe abstraite (un client ne peut être qu’un abonné ou un occasionnel) sans nécessiter d’attribut supplémentaire.

## 2. Comptes de Fidélité

__La classe Compte_Fidelite possèdent les attributs :__

>- id_fidelite : ajout d’une clé artificielle  
>- date_creation : date de la création du compte  
>- points : calcul des points accumulés sur le compte  
>- niveau : niveau du statut de fidélité {bronze, argent, or} offre   différents avantages  
 > 
  >Un client qui est un abonné peut avoir au plus un compte de fidélité (0 ou 1). Un compte de fidélité est composé d’un ou plusieurs abonnements, mais la classe Abonnement peut exister indépendamment de la classe Compte_Fidelite : on les relie donc par une agrégation (losange blanc).
  >
  >De plus, nous avons implémenté une énumération NIVEAU qui permet de distinguer trois statuts de fidélité dans le compte (bronze, argent et or) qui permettront à celui qui possède la compte d’avoir certains avantages tarifaires que nous devrons déterminer.

## 3. Transaction

__La classe Transaction possède les attributs suivants__

>- num_transaction : le numéro de la transaction qui sera unique
>- date_transaction : la date de la transaction
>- moyen_paiement : spécifie le type de paiement utilisé {carte, guichet}

> La classe transaction est une classe mère, ses deux classes filles sont Carte_Abonnement et Ticket. Cette classe permet de garer une trace de toutes les transactions qui ont été effectuées.

## 4. Carte Abonnement

__La classe Carte_Abonnement possèdent les attributs :__

>- id_abonnement : ajout d’une clé artificielle  
>- data_de_debut : création de l’abonnement  
>- date_de_fin : fin de l’abonnement  
  >
  > La carte_abonnement est lié à une zone, donc un utilisateur peut avoir plusieurs cartes d’abonnement en fonction des différentes zones où il gare un de ses véhicules. La relation avec la table Zone va possiblement changer, nous attendons les réponses de notre chargé de TD.

## 5. Ticket

__La classe Ticket possède les attributs suivants:__ 

>- id_ticket : ajout d’une clé artificielle  
>- debut : heure du début de la réservation  
>- fin : heure de fin de la réservation  
>- date : date de l’achat du ticket  
 
>
  >Les tickets sont associés aux occasionnels, qui peuvent payer via différents moyens que l’on a traduit par une énumération (MOYEN_PAIEMENT). Nous avons également noté que la durée des tickets doit être calculée à partir de l'heure de début. Ces tickets ne sont pas reliés directement à un occasionnel mais à son véhicule ce qui empêche un occasionnel d’avoir un ticket pour plusieurs véhicules.


## 6. Gestion des Places dans les Parkings

__La classe Parking possède les attributs suivants :__

>- id_parking : ajout d’une clé artificielle  
>- nom : nom du parking  
>- adresse : l’adresse du parking  
>- nombre_places : le nombre de places total du parking  
> 
>  Nous avons mis en place une contrainte pour garantir que le nombre de places dans chaque parking soit strictement supérieur à zéro. Cette contrainte empêche la création de parkings vides, ce qui pourrait engendrer des incohérences pour la suite dans la base de données.

## 7. Véhicule

__La classe véhicule possède les attributs suivants :__

>- immatriculation : un véhicule est identifié par sa plaque d’immatriculation  
>- type_vehicule	: le type de véhicule  
 >> Pour modéliser les différents types de véhicules, nous utilisons des énumérations pour représenter les types autorisés dans les parkings, qui incluent :  
>>- deux-roues  
>>- camion  
>>- voiture  
>
>  L’énumération permet d'assurer que seules les valeurs valides peuvent être assignées au type de véhicule dans la table Vehicule.


## 8. Réservations

__La classe Reservation est une classe association entre les clients et les places, elle possède les attributs suivants:__

>- id_reservation : ajout d'une clé artificielle
>- date_debut : la date du début de la réservation
>- la date de fin de la réservation

>On permet aux abonnés et aux occasionnels de réserver des places. Les réservations sont indiquées dans la classe Place, avec l'attribut statut qui indique l'état de chaque place (libre, occupée, réservée) grâce à une énumération (STATUT).

## 9. Gestion des Zones

__La classe Zone possède les attributs suivants:__

>- id_zone : ajout d’une clé artificielle  
>- nom : le nom de la zone (centre-ville, zone industrielle, etc )   
>- tarif : le tarif dépend de la zone où est situé le parking  
 > Les zones déterminent les tarifs des parkings qui sont dans ces zones-là. Les modifications de tarifs sont donc possibles via cette classe.


## Incertitudes et ajustement à faire:

__Gestion des abonnements:__ dans notre UML un abonnement est relié à une zone et un abonné: l’abonnement englobe-t-il donc tous les véhicules d’un abonné?  
L’abonnement est-il vraiment relié à une zone ?

__Réservation des places:__ se fait via la classe Place mais comment notifier qu’une place est réservée par un client en particulier ? Comment mettre en place un compteur des places restantes qui empêchera de réserver une place si toutes les places d’un parking sont réservées ou occupées afin d’éviter des problèmes dans la base de données plus tard ? 

Ces problématiques ne peuvent pas être représentées sur un diagramme uml, puisqu’il faut des informations de différentes classes en même temps.  Il faudra les gérer avec des tests en requêtes. 

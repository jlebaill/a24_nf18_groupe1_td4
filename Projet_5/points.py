def ajouter_points_fidelite(cur, conn, login_client, points_a_ajouter):
    """
    Ajoute des points au compte fidélité d'un abonné et met à jour son niveau si nécessaire.
    """
    try:
        # Récupère le compte fidélité de l'abonné
        cur.execute("""
            SELECT id_fidelite, points, niveau
            FROM Compte_Fidelite
            WHERE login_abonne = %s;
        """, (login_client,))
        compte = cur.fetchone()

        if not compte:
            print("Erreur : Aucun compte fidélité trouvé pour cet abonné.")
            return

        id_fidelite, points_actuels, niveau_actuel = compte

        # Ajoute les points
        nouveaux_points = points_actuels + points_a_ajouter

        # Détermine le nouveau niveau en fonction des seuils
        if nouveaux_points >= 400:
            nouveau_niveau = 'or'
        elif nouveaux_points >= 200:
            nouveau_niveau = 'argent'
        else:
            nouveau_niveau = 'bronze'

        # Met à jour les points et le niveau dans la base
        cur.execute("""
            UPDATE Compte_Fidelite
            SET points = %s, niveau = %s
            WHERE login_abonne = %s;
        """, (nouveaux_points, nouveau_niveau, login_client))

        # Valide les changements
        conn.commit()
        print(f"Points ajoutés avec succès ! Vous avez maintenant {nouveaux_points} points et le niveau {nouveau_niveau}.")

    except Exception as e:
        conn.rollback()
        print("Erreur lors de l'ajout des points :", e)


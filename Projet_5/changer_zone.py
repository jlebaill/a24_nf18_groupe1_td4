import psycopg2

def changer_parking_de_zone(cursor, connection):
    # Afficher les parkings disponibles
    query_parkings = """
    SELECT p.id_parking, p.nom, z.nom AS zone_actuelle
    FROM Parking p
    JOIN Zones z ON p.zone_parking = z.id_zone;
    """
    cursor.execute(query_parkings)
    parkings = cursor.fetchall()
    print("\n=== Liste des Parkings ===")
    for parking in parkings:
        print(f"ID: {parking[0]}, Nom: {parking[1]}, Zone actuelle: {parking[2]}")

    try:
        # Sélectionner le parking à modifier
        id_parking = int(input("\nEntrez l'ID du parking que vous souhaitez changer de zone : "))
        # Vérifier si l'ID est valide
        if not any(p[0] == id_parking for p in parkings):
            print("ID de parking invalide. Veuillez réessayer.")
            return
        
        # Afficher les zones disponibles
        query_zones = "SELECT id_zone, nom FROM Zones;"
        cursor.execute(query_zones)
        zones = cursor.fetchall()
        print("\n=== Liste des Zones ===")
        for zone in zones:
            print(f"ID: {zone[0]}, Nom: {zone[1]}")

        # Sélectionner la nouvelle zone
        id_zone = int(input("\nEntrez l'ID de la nouvelle zone : "))
        # Vérifier si l'ID est valide
        if not any(z[0] == id_zone for z in zones):
            print("ID de zone invalide. Veuillez réessayer.")
            return
        
        # Mettre à jour la zone du parking dans la base de données
        query_update = "UPDATE Parking SET zone_parking = %s WHERE id_parking = %s;"
        cursor.execute(query_update, (id_zone, id_parking))
        connection.commit()
        
        print(f"\nLe parking avec l'ID {id_parking} a été déplacé dans la zone {id_zone}.")
    except ValueError:
        print("Erreur : veuillez entrer un nombre valide.")
    except Exception as e:
        print(f"Erreur : {e}")

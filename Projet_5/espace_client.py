import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

def visualiser_espace_client(login_client):
    """
    Affiche les informations du client en fonction de son type (abonné ou occasionnel).
    """
    try:
        # Récupère les informations générales du client
        cur.execute("""
            SELECT c.nom, c.prenom
            FROM Client c
            WHERE c.login_client = %s;
        """, (login_client,))
        client_info = cur.fetchone()

        if not client_info:
            print("Erreur : Aucun client trouvé avec ce login.")
            return

        nom, prenom = client_info
        print("\n=== Espace Client ===")
        print(f"Nom : {nom}")
        print(f"Prénom : {prenom}")
        print(f"Login : {login_client}")

        # Vérifie si le client est un abonné
        cur.execute("""
            SELECT a.login_abonne
            FROM Abonne a
            WHERE a.login_abonne = %s;
        """, (login_client,))
        abonne = cur.fetchone()

        if abonne:
            print("\n--- Statut : Abonné ---")

            # Affiche les infosde compte fidélité
            cur.execute("""
                SELECT cf.points, cf.niveau
                FROM Compte_Fidelite cf
                WHERE cf.login_abonne = %s;
            """, (login_client,))
            compte_fidelite = cur.fetchone()

            if compte_fidelite:
                points, niveau = compte_fidelite
                print(f"Compte Fidélité - Points : {points}, Niveau : {niveau.capitalize()}")

            # Affiche les cartes d'abonnement associées
            cur.execute("""
                SELECT ca.login_abonne, ca.date_de_debut, ca.date_de_fin, ca.moyen_paiement, ca.zone
                FROM Carte_Abonnement ca
                WHERE ca.login_abonne = %s;
            """, (login_client,))
            cartes_abonnement = cur.fetchall()

            if cartes_abonnement:
                print("\n--- Cartes d'Abonnement ---")
                for carte in cartes_abonnement:
                    id_carte, date_debut, date_fin, paiement, zone = carte
                    print(f"ID Carte : {id_carte}")
                    print(f"  Date début : {date_debut}")
                    print(f"  Date fin : {date_fin}")
                    print(f"  Moyen de paiement : {paiement}")
                    print(f"  Zone : {zone}")
            else:
                print("Aucune carte d'abonnement trouvée.")

        else:
            print("\n--- Statut : Occasionnel ---")
            print("Pas de compte fidélité ni de cartes d'abonnement.")

    except Exception as e:
        print("Erreur lors de la visualisation de l'espace client :", e)
    conn.close()

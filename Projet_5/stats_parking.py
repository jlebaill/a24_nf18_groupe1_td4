import psycopg2

# Fonction pour afficher les statistiques des parkings
def afficher_stats_parkings(cursor):
    query = """
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_total_places,
        SUM(CASE WHEN pl.statut = 'libre' THEN 1 ELSE 0 END) AS nb_places_libres,
        SUM(CASE WHEN pl.statut = 'occupee' THEN 1 ELSE 0 END) AS nb_places_occupees,
        ROUND(SUM(CASE WHEN pl.statut = 'occupee' THEN 1.0 ELSE 0 END) / COUNT(pl.id_place) * 100, 2) AS taux_occupation
    FROM Parking pa
    JOIN Place pl ON pl.parking = pa.id_parking
    GROUP BY pa.nom;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Statistiques sur les Parkings ===")
    for row in results:
        print(f"Parking {row[0]} :")
        print(f"  - Total : {row[1]} places")
        print(f"  - Libres : {row[2]} places")
        print(f"  - Occupées : {row[3]} places")
        print(f"  - Taux d'occupation : {row[4]}%")

    # Taux d'occupation global
    query_global = """
    SELECT 
        ROUND(SUM(CASE WHEN pl.statut = 'occupee' THEN 1.0 ELSE 0 END) / COUNT(pl.id_place) * 100, 2) AS taux_occupation_global
    FROM Place pl;
    """
    cursor.execute(query_global)
    taux_occupation_global = cursor.fetchone()[0]
    print("\nTaux d'occupation global : {:.2f}%".format(taux_occupation_global))

    # Moyenne des réservations par place
    query_reservations = """
    SELECT 
        pa.nom AS nom_parking,
        ROUND(COUNT(r.id_reservation) * 1.0 / COUNT(pl.id_place), 2) AS moy_reservations_par_place
    FROM Parking pa
    JOIN Place pl ON pa.id_parking = pl.parking
    LEFT JOIN Reservation r ON pl.id_place = r.place_reservee
    GROUP BY pa.nom;
    """
    cursor.execute(query_reservations)
    reservations_par_place = cursor.fetchall()
    print("\n=== Moyenne des Réservations par Parking ===")
    for row in reservations_par_place:
        print(f"Parking {row[0]} : {row[1]} réservations en moyenne par place")

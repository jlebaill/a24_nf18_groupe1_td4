import psycopg2

def mettre_a_jour_tarif_zone(cursor, connection):
    # Liste des zones pour guider le gérant
    query_zones = "SELECT id_zone, nom, tarif_horaire, tarif_abo FROM Zones;"
    cursor.execute(query_zones)
    zones = cursor.fetchall()
    print("\n=== Liste des Zones ===")
    for zone in zones:
        print(f"ID: {zone[0]}, Nom: {zone[1]}, Tarif horaire: {zone[2]} €, Tarif abonnement: {zone[3]} €")

    try:
        # Demande à l'utilisateur de choisir une zone
        id_zone = int(input("\nEntrez l'ID de la zone que vous souhaitez modifier : "))
        # Vérifie que l'ID existe
        if not any(z[0] == id_zone for z in zones):
            print("ID de zone invalide. Veuillez réessayer.")
            return
        
        # Demande au gérant s'il veut modifier le tarif horaire ou le tarif d'abonnement
        choix = input("Souhaitez-vous modifier le tarif horaire (H) ou le tarif d'abonnement (A) ? ").strip().upper()
        if choix == "H":
            nouveau_tarif_horaire = float(input("Entrez le nouveau tarif horaire (€) : "))
            # Mise à jour dans la base de données
            query_update_horaire = "UPDATE Zones SET tarif_horaire = %s WHERE id_zone = %s;"
            cursor.execute(query_update_horaire, (nouveau_tarif_horaire, id_zone))
            print(f"Le tarif horaire de la zone avec l'ID {id_zone} a été mis à jour à {nouveau_tarif_horaire} €.")
        elif choix == "A":
            nouveau_tarif_abo = float(input("Entrez le nouveau tarif d'abonnement (€) : "))
            # Mise à jour dans la base de données
            query_update_abo = "UPDATE Zones SET tarif_abo = %s WHERE id_zone = %s;"
            cursor.execute(query_update_abo, (nouveau_tarif_abo, id_zone))
            print(f"Le tarif d'abonnement de la zone avec l'ID {id_zone} a été mis à jour à {nouveau_tarif_abo} €.")
        else:
            print("Choix invalide. Veuillez entrer 'H' ou 'A'.")
            return
        
        # Valider la mise à jour
        connection.commit()
    except ValueError:
        print("Erreur : veuillez entrer un nombre valide.")
    except Exception as e:
        print(f"Erreur : {e}")

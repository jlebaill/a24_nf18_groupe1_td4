import psycopg2
from client import *
from points import *
from abonne import *
from carte_abonnement import *
from espace_client import *
from parking import *
from transaction import *
from vehicule import *
from stats_clients import *
from stats_parking import *
from stats_fidelite import *
from maj_tarif import *
from changer_zone import *

conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

# Menu pour le gérant
def menu_gerant():
    choix = 0
    while choix != 6:
        print("\n=== Menu Gérant ===")
        print("1. Statistiques sur les clients")
        print("2. Statistiques sur les comptes de fidélité")
        print("3. Statistiques sur les parkings")
        print("4. Mettre à jour les tarifs d'une zone")
        print("5. Changer un parking de zone")
        print("6. Quitter le menu gérant")
        try:
            choix = int(input("Votre choix : "))
            match choix:
                case 1:
                    afficher_stats_clients(cur)
                case 2:
                    afficher_stats_points_fidelite(cur)
                    afficher_comptes_fidelite(cur)
                case 3:
                    afficher_stats_parkings(cur)
                case 4:
                    mettre_a_jour_tarif_zone(cur, conn)
                case 5:
                    changer_parking_de_zone(cur, conn)
                case 6:
                    print("\nRetour au menu principal.\n")
                case _:
                    print("\nERREUR : votre choix n'est pas valide ! ")
        except ValueError:
            print("Erreur : veuillez entrer un nombre.")

# Menu pour l'utilisateur
def menu_utilisateur():
    login_client = input("Entrez votre login pour vous connecter (vide si nouveau client): ")
    if not login_client:
        login_client = creer_compte()
    if not verifier_login(login_client):
        print("Login invalide. Création d'un nouveau compte.")
        login_client = creer_compte()

    choix = 0
    while choix != 7:
        print("\n ======================================")
        print("\n 1. Créer un compte client")
        print("\n 2. Devenir abonné")
        print("\n 3. Acheter une nouvelle carte d'abonnement")
        print("\n 4. Réserver une place de parking")
        print("\n 5. Enregistrer un de ses véhicules")
        print("\n 6. Visualiser mon Espace Client")
        print("\n 7. Acheter un ticket")
        print("\n 8. Déconnexion")
        print("\n ======================================")

        try:
            choix = int(input("Saisissez votre choix : "))
            match choix:
                case 1:
                    creer_compte()
                case 2:
                    devenir_abonne(login_client)
                case 3:
                    ajouter_carte_abonnement(login_client)   
                case 4:
                    reserver_place(login_client)
                case 5:
                    enregistrer_vehicule(login_client)
                case 6:
                    visualiser_espace_client(login_client)
                case 7:
                    acheter_ticket(login_client)
                case 8:
                    print("\nRetour au menu principal.\n")
                    menu_principal()
                case _:
                    print("\n\nERREUR : votre choix n'est pas valide ! ")
        except ValueError:
            print("Erreur : veuillez entrer un nombre.")

# Fonction principale pour choisir entre gérant ou utilisateur
def menu_principal():
    while True:  # Boucle pour revenir au menu principal après quitter un sous-menu
        print("\n=== Menu Principal ===")
        print("Bienvenue dans le système de gestion de parking.")
        print("1. Gérant")
        print("2. Utilisateur")
        print("3. Quitter")
        role = input("Entrez votre choix : ")
        if role == "1":
            login = input("Entrez votre login (admin requis) : ")
            password = input("Entrez votre mot de passe : ")
            if login == "admin" and password == "admin":
                menu_gerant()
            else:
                print("Login ou mot de passe incorrect. Accès refusé.")
        elif role == "2":
            menu_utilisateur()
        elif role == "3":
            print("Au revoir! Fin du programme.")
            break  # Quitte la boucle et termine le programme
        else:
            print("Choix invalide. Veuillez réessayer.")

if __name__ == "__main__":
    try:
        menu_principal()
    finally:
        conn.close()
import psycopg2
from points import *
from datetime import date
from decimal import Decimal 

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

def ajouter_carte_abonnement(login_client):
    try:
        # Vérifie si le compte fidélité existe
        cur.execute("""
            SELECT id_fidelite, niveau 
            FROM Compte_Fidelite 
            WHERE login_abonne = %s
        """, (login_client,))
        compte_fidelite = cur.fetchone()

        if not compte_fidelite:
            print("Erreur : Aucun compte fidélité trouvé. Vous devez en créer un avant.")
            return
        
        # On récupère l'id du compte fidélité et le niveau
        id_fidelite, niveau_fidelite = compte_fidelite
        print(f"Compte fidélité trouvé. ID : {id_fidelite}, Niveau : {niveau_fidelite}")

        # Affiche les zones avec les tarifs réduits
        print("\nZones disponibles avec tarifs ajustés :")

        # Applique les réductions selon le niveau
        if niveau_fidelite == 'bronze':
            reduction = Decimal(0.95)
        elif niveau_fidelite == 'argent':
            reduction = Decimal(0.85)
        elif niveau_fidelite == 'or':
            reduction = Decimal(0.75)
        else:
            reduction = Decimal(1.0)  # Pas de réduction par défaut

        # Récupère toutes les zones
        cur.execute("SELECT id_zone, nom, tarif_abo FROM Zones")
        zones = cur.fetchall()

        # Affiche chaque zone avec le tarif réduit
        for zone in zones:
            id_zone, nom, tarif_base = zone
            tarif_reduit = tarif_base * reduction
            print(f"ID : {id_zone} | Zone : {nom} | Tarif : {tarif_reduit:.2f} €")

        # Demande à l'utilisateur de choisir une zone
        zone_id = int(input("\nChoisissez une zone (ID) : "))
        cur.execute("SELECT * FROM Zones WHERE id_zone = %s", (zone_id,))
        selected_zone = cur.fetchone()

        if not selected_zone:
            print("Erreur : Zone invalide.")
            return

        # Crée la carte d'abonnement
        print("Création de la carte d'abonnement...")

        # Trouve le prochain id d'abonnement
        cur.execute("SELECT MAX(id_abonnement) FROM Carte_Abonnement")
        max_id_abonnement = cur.fetchone()[0]
        if max_id_abonnement is None:
            max_id_abonnement = 0  # Si aucun abonnement n'existe encore
        new_id_abonnement = max_id_abonnement + 1

        cur.execute("SELECT MAX(num_transaction) FROM Carte_Abonnement")
        max_num_transaction = cur.fetchone()[0]
        if max_num_transaction is None:
            max_num_transaction = 0  # Si aucun numéro de transaction n'existe encore
        new_num_transaction = max_num_transaction + 1

        # Dates et infos pour la carte
        date_de_debut = date.today()
        date_de_fin = date(date_de_debut.year + 1, date_de_debut.month, date_de_debut.day)
        moyen_paiement = input("Entrez le moyen de paiement (carte/guichet) : ")

        # Ajoute la carte dans la base
        cur.execute("""
            INSERT INTO Carte_Abonnement (id_abonnement, date_de_debut, date_de_fin, compte_fidelite, login_abonne, num_transaction, date_transaction, moyen_paiement, zone)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s)
        """, (new_id_abonnement, date_de_debut, date_de_fin, id_fidelite, login_client,new_num_transaction, date_de_debut, moyen_paiement, zone_id))

        print(f"Carte d'abonnement ajoutée avec succès. ID : {new_id_abonnement}")

        ajouter_points_fidelite(cur, conn, login_client, points_a_ajouter=20)


        # Sauvegarde les changements
        conn.commit()

    except Exception as e:
        print("Erreur :", e)
        if conn:
            conn.rollback()
            
    conn.close()


import psycopg2
from points import *
from datetime import date
from decimal import Decimal
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

def devenir_abonne(login_client):
    try:
        # Connexion à la base de données
        with psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'") as conn:
            with conn.cursor() as cur:
                # Crée un compte fidélité
                print("Création d'un compte fidélité...")

                cur.execute("""
                    SELECT id_fidelite, niveau 
                    FROM Compte_Fidelite 
                    WHERE login_abonne = %s
                """, (login_client,))
                compte_fidelite = cur.fetchone()

                if compte_fidelite:
                    print("Erreur : Vous avez déjà un compte fidelité.")
                    return

                cur.execute("SELECT MAX(id_fidelite) FROM Compte_Fidelite")
                max_id_fidelite = cur.fetchone()[0]
                if max_id_fidelite is None:
                    max_id_fidelite = 0  # Si aucun compte fidélité n'existe encore
                new_id_fidelite = max_id_fidelite + 1

                date_creation = date.today()
                points = 0
                niveau = 'bronze'

                cur.execute("""
                    INSERT INTO Compte_Fidelite (id_fidelite, date_creation, points, niveau, login_abonne)
                    VALUES (%s, %s, %s, %s, %s) RETURNING id_fidelite;
                """, (new_id_fidelite, date_creation, points, niveau, login_client))
                id_fidelite = cur.fetchone()[0]
                print(f"Compte fidélité créé avec succès. ID : {id_fidelite}")

                # Insérer dans Abonne s'il n'existe pas encore
                cur.execute("""
                    INSERT INTO Abonne (login_abonne) 
                    VALUES (%s) 
                    ON CONFLICT (login_abonne) DO NOTHING;
                """, (login_client,))

                # Propose les zones avec tarifs ajustés
                print("Voici les zones disponibles avec les tarifs ajustés selon votre niveau de fidélité :")

                cur.execute("""
                    SELECT z.id_zone, z.nom, z.tarif_abo, cf.niveau
                    FROM Zones z
                    CROSS JOIN (SELECT niveau FROM Compte_Fidelite WHERE login_abonne = %s) cf;
                """, (login_client,))
                zones = cur.fetchall()

                for zone in zones:
                    id_zone, nom, tarif_base, niveau = zone
                    reduction = Decimal(1.0)  # Par défaut, pas de réduction
                    if niveau == 'argent':
                        reduction = Decimal(0.85)
                    elif niveau == 'or':
                        reduction = Decimal(0.75)
                    elif niveau == 'bronze':
                        reduction = Decimal(0.95)

                    tarif_reduit = tarif_base * reduction
                    print(f"ID Zone : {id_zone} | Nom : {nom} | Tarif avec réduction ({niveau}): {tarif_reduit:.2f} €")

                # Choisis une zone
                zone_id = int(input("Choisissez une zone (ID Zone) : "))

                # Vérifie si la zone existe
                cur.execute("SELECT * FROM Zones WHERE id_zone = %s;", (zone_id,))
                selected_zone = cur.fetchone()
                if not selected_zone:
                    print("Erreur : Zone non valide.")
                    return

                # Crée une carte d'abonnement
                print("Création de la carte d'abonnement...")

                cur.execute("SELECT MAX(id_abonnement) FROM Carte_Abonnement")
                max_id_abonnement = cur.fetchone()[0]
                if max_id_abonnement is None:
                    max_id_abonnement = 0  # Si aucun abonnement n'existe encore
                new_id_abonnement = max_id_abonnement + 1

                cur.execute("SELECT MAX(num_transaction) FROM Carte_Abonnement")
                max_num_transaction = cur.fetchone()[0]
                if max_num_transaction is None:
                    max_num_transaction = 0  # Si aucun numéro de transaction n'existe encore
                new_num_transaction = max_num_transaction + 1

                date_de_debut = date.today()
                date_de_fin = date(date_de_debut.year + 1, date_de_debut.month, date_de_debut.day)  # 1 an
                moyen_paiement = input("Entrez le moyen de paiement (carte/guichet) : ")

                cur.execute("""
                    INSERT INTO Carte_Abonnement (id_abonnement, date_de_debut, date_de_fin, compte_fidelite, login_abonne, num_transaction, date_transaction, moyen_paiement, zone)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id_abonnement;
                """, (new_id_abonnement, date_de_debut, date_de_fin, id_fidelite, login_client, new_num_transaction, date_de_debut, moyen_paiement, zone_id))
                id_abonnement = cur.fetchone()[0]

                print(f"Carte d'abonnement créée avec succès. ID : {id_abonnement}")

                # Ajout des points fidélité pour la création de la carte
                ajouter_points_fidelite(cur, conn, login_client, points_a_ajouter=20)

    except Exception as e:
        print("Une erreur s'est produite :", e)


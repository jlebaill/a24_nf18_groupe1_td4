import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

# Fonction 1: Enregistrer un véhicule
def enregistrer_vehicule(login_client):
    immatriculation = input("Entrez l'immatriculation du véhicule : ")
    type_vehicule = input("Entrez le type de véhicule (deux_roues, camion, voiture) : ")

    try:
        cur.execute("INSERT INTO Vehicule (immatriculation, type_de_vehicule,ticket_vehicule, login_client) VALUES (%s, %s, %s, %s)",
                    (immatriculation, type_vehicule,0, login_client))
        conn.commit()
        print("Véhicule enregistré avec succès.")

    except Exception as e:
        conn.rollback()
        print(f"Erreur lors de l'ajout du véhicule: {e}")

    conn.close()
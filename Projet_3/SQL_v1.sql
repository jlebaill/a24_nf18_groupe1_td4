DROP VIEW IF EXISTS NbCarteParAbonnee;
DROP VIEW IF EXISTS VehiculeParClient ;
DROP VIEW IF EXISTS NumTransactionDifferent;

DROP TABLE IF EXISTS Reservation;
DROP TABLE IF EXISTS Vehicule;
DROP TABLE IF EXISTS Ticket;
DROP TABLE IF EXISTS Place;
DROP TABLE IF EXISTS Parking;
DROP TABLE IF EXISTS Zones;
DROP TABLE IF EXISTS Compte_Fidelite;
DROP TABLE IF EXISTS Abonne;
DROP TABLE IF EXISTS Occasionnel;
DROP TABLE IF EXISTS Client;

-- Creation de table

CREATE TABLE Client (
	login_client VARCHAR(10) PRIMARY KEY, 
	nom VARCHAR(20) NOT NULL, 
	prenom VARCHAR(20) NOT NULL
);


CREATE TABLE Occasionnel (
	login_occasionnel VARCHAR REFERENCES Client(login_client) PRIMARY KEY
);


CREATE TABLE Abonne(
	login_abonne VARCHAR REFERENCES Client(login_client) PRIMARY KEY
);


CREATE TABLE Compte_Fidelite (
	id_fidelite INTEGER PRIMARY KEY , 
	date_creation DATE NOT NULL, 
	points INTEGER NOT NULL, 
	niveau VARCHAR CHECK (niveau IN ('bronze', 'argent', 'or')),
	login_abonne VARCHAR REFERENCES Client(login_client) UNIQUE NOT NULL
); 


CREATE TABLE Zones (
	id_zone INTEGER PRIMARY KEY,
	nom VARCHAR NOT NULL,
	tarif_horaire DECIMAL(5,2) NOT NULL,
	tarif_abo DECIMAL(5,2) NOT NULL
);


CREATE TABLE Carte_Abonnement (
	id_abonnement INTEGER PRIMARY KEY,
	date_de_debut DATE NOT NULL, 
	date_de_fin DATE NOT NULL, 
	compte_fidelite INTEGER REFERENCES Compte_Fidelite(id_fidelite) NOT NULL, 
	login_abonne VARCHAR REFERENCES Abonne(login_abonne) NOT NULL, 
	num_transaction INTEGER UNIQUE NOT NULL, 
	date_transaction DATE NOT NULL, 
	moyen_paiement VARCHAR CHECK (moyen_paiement IN ('carte','guichet')),
	zone INTEGER REFERENCES Zones(id_zone) NOT NULL 
); 


CREATE TABLE Parking (
	id_parking INTEGER PRIMARY KEY,
	nom VARCHAR NOT NULL,
	adresse VARCHAR NOT NULL,
	nombre_places INTEGER NOT NULL,
	zone_parking INTEGER REFERENCES Zones(id_zone) NOT NULL
);


CREATE TABLE Place (
	id_place INTEGER PRIMARY KEY,
	numero VARCHAR NOT NULL,
	statut VARCHAR CHECK (statut IN ('libre', 'occupee', 'reservee')) NOT NULL,
	type_de_vehicule VARCHAR CHECK (type_de_vehicule IN ('deux_roues', 'camion', 'voiture')) NOT NULL,
type_zone VARCHAR CHECK (type_zone IN ('couverte', 'plein_air')) NOT NULL,
parking INTEGER REFERENCES Parking(id_parking) NOT NULL
);


CREATE TABLE Ticket (
	id_ticket INTEGER PRIMARY KEY,
	debut TIME NOT NULL,
	fin TIME NOT NULL,
	date DATE NOT NULL,
	num_transaction INTEGER UNIQUE NOT NULL,
	moyen_paiement VARCHAR CHECK (moyen_paiement IN ('carte', 'guichet')) NOT NULL,
	zone INTEGER REFERENCES Zones(id_zone) NOT NULL
);


CREATE TABLE Vehicule (
	immatriculation VARCHAR(9) PRIMARY KEY,
	type_de_vehicule VARCHAR CHECK (type_de_vehicule IN ('deux_roues', 'camion', 'voiture')) NOT NULL,
	ticket_vehicule INTEGER REFERENCES Ticket(id_ticket) NOT NULL,
	login_client VARCHAR REFERENCES Client(login_client) NOT NULL
);


CREATE TABLE Reservation (
login_client VARCHAR REFERENCES Client(login_client) NOT NULL,
place_reservee INTEGER REFERENCES Place(id_place) NOT NULL,
id_reservation INTEGER NOT NULL, 
date_debut DATE NOT NULL, 
date_fin DATE NOT NULL,
PRIMARY KEY (login_client, place_reservee, id_reservation)
);

-- Expression des contraintes

CREATE VIEW NbCarteParAbonnee AS
SELECT login_abonne 
FROM Abonne 
EXCEPT
SELECT login_abonne 
FROM Carte_Abonnement;


-- Permet de vérifier
-- Projection(Abonne, id_abonne)=Projection(Carte_Abonnement, id_abonnement)
-- en donnant la liste des Abonnées ne respectant pas la contrainte, c’est à dire qu’ils ne possèdent pas de carte abonnement(si la base de données est remplies correctement, la vue n'affiche rien)


CREATE VIEW VehiculeParClient AS
SELECT login_client 
FROM Client
EXCEPT
SELECT DISTINCT login_client 
FROM Vehicule;


-- Permet de vérifier
-- Projection(Client, id_client)=Projection(Vehicule, id_client)
-- en donnant la liste des Clients ne respectant pas la contrainte, c’est à dire qu’ils ne possèdent pas de véhicule(si la base de données est remplies correctement, la vue n'affiche rien)


CREATE VIEW NumTransactionDifferent AS
SELECT t.num_transaction
FROM Ticket t
JOIN Carte_Abonnement c
ON t.num_transaction = c.num_transaction;


-- Permet de vérifier
-- Intersection(Projection(Ticket, num_transaction), Projection(Carte_Abonnement, num_transaction)={}
-- en donnant la liste des transactions ne respectant pas la contrainte, c’est à dire les transactions qui ont le même numéro dans Ticket et dans Carte_ABonnement(si la base de données est remplies correctement, la vue n'affiche rien)


-- Insert dans la BDD


INSERT INTO Client VALUES('p.dupont','Pierre','Dupont');
INSERT INTO Client VALUES('l.colt','Louise','Colt');
INSERT INTO Client VALUES('s.jahar','Stephane','Jahar');
INSERT INTO Client VALUES('c.char','Christian','Charenton');
INSERT INTO Client VALUES('j.lebailly','Jeanne', 'Le Bailly');
INSERT INTO Client VALUES('s.efendi', 'Sarah', 'Efendioglu');
INSERT INTO Client VALUES('j.vives', 'Jean', 'Vives');
INSERT INTO Client VALUES('c.roth', 'Clémence', 'Roth');


INSERT INTO Occasionnel VALUES('p.dupont');
INSERT INTO Occasionnel VALUES('c.char');
INSERT INTO Occasionnel VALUES('j.lebailly');
INSERT INTO Occasionnel VALUES('s.efendi');


INSERT INTO Abonne VALUES('l.colt');
INSERT INTO Abonne VALUES('s.jahar');
INSERT INTO Abonne VALUES('j.vives');
INSERT INTO Abonne VALUES('c.roth');


INSERT INTO Compte_Fidelite VALUES(1,TO_DATE('YYYY-MM-DD', '2023-10-02'), 58, 'bronze', 'l.colt');
INSERT INTO Compte_Fidelite VALUES(2,TO_DATE('YYYY-MM-DD', '2021-03-01'), 183, 'argent', 's.jahar');
INSERT INTO Compte_Fidelite VALUES(3,TO_DATE('YYYY-MM-DD', '2022-11-10'), 12, 'bronze', 'j.vives');
INSERT INTO Compte_Fidelite VALUES(4,TO_DATE('YYYY-MM-DD', '2023-07-15'), 243, 'or', 'c.roth');



INSERT INTO Zones VALUES(1,'Rouge', 8.0,  320.0);
INSERT INTO Zones VALUES(2,'Orange', 6.0,  280.0);
INSERT INTO Zones VALUES(3,'Jaune', 4.0, 220.0 );
INSERT INTO Zones VALUES(4,'Vert', 2.0,  180.0);


INSERT INTO Carte_Abonnement VALUES (1, TO_DATE('YYYY-MM-DD', '2024-08-02'),TO_DATE('YYYY-MM-DD', '2024-09-02'),1, 'l.colt',2, TO_DATE('YYYY-MM-DD', '2024-08-01'),'carte', 4);
INSERT INTO Carte_Abonnement VALUES (2, TO_DATE('YYYY-MM-DD', '2024-06-10'),TO_DATE('YYYY-MM-DD', '2024-07-10'),2, 's.jahar',4, TO_DATE('YYYY-MM-DD', '2024-06-02'),'guichet', 2);
INSERT INTO Carte_Abonnement VALUES (3, TO_DATE('YYYY-MM-DD', '2024-10-02'),TO_DATE('YYYY-MM-DD', '2024-11-20'),3, 'j.vives',6, TO_DATE('YYYY-MM-DD', '2024-12-20'),'carte', 1);
INSERT INTO Carte_Abonnement VALUES (4, TO_DATE('YYYY-MM-DD', '2024-10-02'),TO_DATE('YYYY-MM-DD', '2024-05-03'),4, 'c.roth',8, TO_DATE('YYYY-MM-DD', '2024-12-20'),'carte', 1);


INSERT INTO Parking VALUES (1, 'David Bo-oui', '30 avenue du Tertre', 150, 1);
INSERT INTO Parking VALUES (2, 'UTCool', '3 place Benjamin Franklin', 50, 3);
INSERT INTO Parking VALUES (3, 'Chaquira', '271 impasse du Lolelole', 200, 1);
INSERT INTO Parking VALUES (4, 'Rattata', '25 rue David Lafarge', 150, 1);
INSERT INTO Parking VALUES (5,'Magick Sistem','24 rue du moulin',269,1);
INSERT INTO Parking VALUES (6,'Mpokorat','12 avenue du pendu',180,2);
INSERT INTO Parking VALUES (7,'Dalmatiens','41 chemin de Cruella',101,3);
INSERT INTO Parking VALUES (8,'Colonel raiyel','25 chemin de celui',70,4);


INSERT INTO Place VALUES (1, 1, 'occupee', 'deux_roues', 'couverte', 1);
INSERT INTO Place VALUES (2, 2, 'libre', 'camion', 'couverte', 1);
INSERT INTO Place VALUES (3, 3, 'reservee', 'deux_roues', 'couverte', 1);
INSERT INTO Place VALUES (4, 1, 'libre', 'voiture', 'plein_air', 2);
INSERT INTO Place VALUES (5, 2, 'libre', 'voiture', 'plein_air', 2);
INSERT INTO Place VALUES (6, 3, 'occupee', 'deux_roues', 'plein_air', 2);
INSERT INTO Place VALUES (7, 1, 'occupee', 'deux_roues', 'plein_air', 3);
INSERT INTO Place VALUES (8, 2, 'occupee', 'voiture', 'couverte', 3);
INSERT INTO Place VALUES (9, 3, 'occupee', 'camion', 'couverte', 3);
INSERT INTO Place VALUES (10, 1, 'occupee', 'deux_roues', 'couverte', 4);
INSERT INTO Place VALUES (11, 2, 'occupee', 'camion', 'couverte', 4);
INSERT INTO Place VALUES (12, 3, 'occupee', 'camion', 'couverte', 4);
INSERT INTO Place VALUES (13,1,'libre','camion','couverte',5);
INSERT INTO Place VALUES (14,2,'occupee','camion','couverte',5);
INSERT INTO Place VALUES (15,3,'occupee','camion','couverte',5);
INSERT INTO Place VALUES (16,1,'libre','deux_roues','couverte',6);
INSERT INTO Place VALUES (17,2,'reservee','deux_roues','couverte',6);
INSERT INTO Place VALUES (18,3,'libre','deux_roues','couverte',6);
INSERT INTO Place VALUES (19,1,'libre','voiture','plein_air',7);
INSERT INTO Place VALUES (20,2,'libre','voiture','plein_air',7);
INSERT INTO Place VALUES (21,3,'libre','camion','plein_air',7);
INSERT INTO Place VALUES (22,1,'occupee','voiture','couverte',8);
INSERT INTO Place VALUES (23,2,'occupee','voiture','plein_air',8);
INSERT INTO Place VALUES (24,3,'occupee','voiture','plein_air',8);


INSERT INTO Ticket VALUES (1,'10:34:53.44','12:34:53.44','2024-11-04',1,'carte',2);
INSERT INTO Ticket VALUES (2,'14:10:00.00','18:10:00.00','2024-11-03',3,'guichet',3);
INSERT INTO Ticket VALUES (3,'8:20:00.00','19:20:00.00','2024-10-16',5,'guichet',1);
INSERT INTO Ticket VALUES (4,'15:00:00.00','17:00:00.00','2024-11-01',7,'carte',4);
INSERT INTO Ticket VALUES (5,'11:30:00.00','15:30:00.00','2024-09-14',9,'carte',2);
INSERT INTO Ticket VALUES (6,'17:10:00.00','21:10:00.00','2024-10-29',11,'guichet',3);
INSERT INTO Ticket VALUES (7,'10:20:00.00','18:20:00.00','2024-08-16',13,'guichet',3);
INSERT INTO Ticket VALUES (8,'14:30:00.00','17:30:00.00','2024-10-01',15,'carte',1);
INSERT INTO Ticket VALUES (9,'12:30:00.00','13:30:00.00','2024-12-01',16,'carte',1);


INSERT INTO Vehicule VALUES ('PL-123-AK', 'voiture', 1,'p.dupont');
INSERT INTO Vehicule VALUES ('MK-158-JF', 'voiture',2,'l.colt');
INSERT INTO Vehicule VALUES ('AZ-562-SL', 'camion',5,'s.jahar');
INSERT INTO Vehicule VALUES ('JI-987-KO', 'deux_roues',8,'p.dupont');
INSERT INTO Vehicule VALUES ('GT-578-OI', 'camion',7,'c.char');
INSERT INTO Vehicule VALUES ('PU-548-YT', 'camion',3,'j.lebailly');
INSERT INTO Vehicule VALUES ('FR-943-DH', 'voiture',4,'s.efendi');
INSERT INTO Vehicule VALUES ('XS-524-VC', 'voiture',6,'j.vives');
INSERT INTO Vehicule VALUES ('OL-094-IC', 'deux_roues',9,'c.roth');



INSERT INTO Reservation VALUES ('c.roth',3,1,TO_DATE('YYYY-MM-DD', '2024-10-02'),TO_DATE('YYYY-MM-DD', '2024-10-03'));
INSERT INTO Reservation VALUES ('j.vives',17,2,TO_DATE('YYYY-MM-DD', '2024-11-22'),TO_DATE('YYYY-MM-DD', '2024-11-23'));
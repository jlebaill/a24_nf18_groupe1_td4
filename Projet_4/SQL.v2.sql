DROP VIEW IF EXISTS NbCarteParAbonnee;
DROP VIEW IF EXISTS VehiculeParClient ;
DROP VIEW IF EXISTS NumTransactionDifferent;

DROP TABLE IF EXISTS Reservation;
DROP TABLE IF EXISTS Vehicule;
DROP TABLE IF EXISTS Ticket;
DROP TABLE IF EXISTS Place;
DROP TABLE IF EXISTS Parking;
DROP TABLE IF EXISTS Zones;
DROP TABLE IF EXISTS Compte_Fidelite;
DROP TABLE IF EXISTS Abonne;
DROP TABLE IF EXISTS Occasionnel;
DROP TABLE IF EXISTS Client;

-- Creation de table

CREATE TABLE Client (
	login_client VARCHAR(10) PRIMARY KEY, 
	nom VARCHAR(20) NOT NULL, 
	prenom VARCHAR(20) NOT NULL
);

CREATE TABLE Occasionnel (
	login_occasionnel VARCHAR(10) PRIMARY KEY REFERENCES Client(login_client)
);

CREATE TABLE Abonne(
	login_abonne VARCHAR(10) PRIMARY KEY REFERENCES Client(login_client)
);

CREATE TABLE Compte_Fidelite (
	id_fidelite INTEGER PRIMARY KEY , 
	date_creation DATE NOT NULL, 
	points INTEGER, 
	niveau VARCHAR(10) CHECK (niveau IN ('bronze', 'argent', 'or')),
	login_abonne VARCHAR(10) UNIQUE NOT NULL REFERENCES Client(login_client)
); 

CREATE TABLE Zones (
	id_zone INTEGER PRIMARY KEY,
	nom VARCHAR(20) NOT NULL,
	tarif_horaire DECIMAL(5,2) NOT NULL,
	tarif_abo DECIMAL(5,2) NOT NULL
);

CREATE TABLE Carte_Abonnement (
	id_abonnement INTEGER PRIMARY KEY,
	date_de_debut DATE NOT NULL, 
	date_de_fin DATE NOT NULL, 
	compte_fidelite INTEGER NOT NULL REFERENCES Compte_Fidelite(id_fidelite),
	login_abonne VARCHAR(10) NOT NULL REFERENCES Abonne(login_abonne), 
	num_transaction INTEGER UNIQUE NOT NULL, 
	date_transaction DATE NOT NULL, 
	moyen_paiement VARCHAR(10) CHECK (moyen_paiement IN ('carte','guichet')),
	zone INTEGER NOT NULL REFERENCES Zones(id_zone)
); 

CREATE TABLE Parking (
	id_parking INTEGER PRIMARY KEY,
	nom VARCHAR(50) NOT NULL,
	adresse VARCHAR(100) NOT NULL,
	nombre_places INTEGER NOT NULL,
	zone_parking INTEGER NOT NULL REFERENCES Zones(id_zone)
);

CREATE TABLE Place (
	id_place INTEGER PRIMARY KEY,
	numero VARCHAR(10) NOT NULL,
	statut VARCHAR(10) CHECK (statut IN ('libre', 'occupee', 'reservee')) NOT NULL,
	type_de_vehicule VARCHAR(10) CHECK (type_de_vehicule IN ('deux_roues', 'camion', 'voiture')) NOT NULL,
	type_zone VARCHAR(10) CHECK (type_zone IN ('couverte', 'plein_air')) NOT NULL,
	parking INTEGER NOT NULL REFERENCES Parking(id_parking)
);

CREATE TABLE Ticket (
	id_ticket INTEGER PRIMARY KEY,
	debut TIME NOT NULL,
	fin TIME NOT NULL,
	date DATE NOT NULL,
	moyen_paiement VARCHAR(10) CHECK (moyen_paiement IN ('carte', 'guichet')) NOT NULL,
	zone INTEGER NOT NULL REFERENCES Zones(id_zone)
);

CREATE TABLE Vehicule (
	immatriculation VARCHAR(9) PRIMARY KEY,
	type_de_vehicule VARCHAR(10) CHECK (type_de_vehicule IN ('deux_roues', 'camion', 'voiture')) NOT NULL,
	ticket_vehicule INTEGER NOT NULL REFERENCES Ticket(id_ticket),
	login_client VARCHAR(10) NOT NULL REFERENCES Client(login_client)
);

CREATE TABLE Reservation (
	login_client VARCHAR(10) NOT NULL REFERENCES Client(login_client),
	place_reservee INTEGER NOT NULL REFERENCES Place(id_place),
	id_reservation INTEGER NOT NULL, 
	date_debut DATE NOT NULL, 
	date_fin DATE NOT NULL,
	PRIMARY KEY (login_client, place_reservee, id_reservation)
);

-- Expression des contraintes

CREATE VIEW NbCarteParAbonnee AS
SELECT login_abonne 
FROM Abonne 
EXCEPT
SELECT login_abonne 
FROM Carte_Abonnement;

-- Permet de vérifier
-- Projection(Abonne, id_abonne)=Projection(Carte_Abonnement, id_abonnement)
-- en donnant la liste des Abonnées ne respectant pas la contrainte, c’est à dire qu’ils ne possèdent pas de carte abonnement(si la base de données est remplies correctement, la vue n'affiche rien)

CREATE VIEW VehiculeParClient AS
SELECT login_client 
FROM Client
EXCEPT
SELECT DISTINCT login_client 
FROM Vehicule;

-- Permet de vérifier
-- Projection(Client, id_client)=Projection(Vehicule, id_client)
-- en donnant la liste des Clients ne respectant pas la contrainte, c’est à dire qu’ils ne possèdent pas de véhicule(si la base de données est remplies correctement, la vue n'affiche rien)

CREATE VIEW NumTransactionDifferent AS
SELECT t.num_transaction
FROM Ticket t
JOIN Carte_Abonnement c
ON t.num_transaction = c.num_transaction;

-- Permet de vérifier
-- Intersection(Projection(Ticket, num_transaction), Projection(Carte_Abonnement, num_transaction)={}
-- en donnant la liste des transactions ne respectant pas la contrainte, c’est à dire les transactions qui ont le même numéro dans Ticket et dans Carte_ABonnement(si la base de données est remplies correctement, la vue n'affiche rien)

-- Insert dans la BDD

INSERT INTO Client VALUES('p.dupont','Pierre','Dupont');
INSERT INTO Client VALUES('l.colt','Louise','Colt');
INSERT INTO Client VALUES('s.jahar','Stephane','Jahar');
INSERT INTO Client VALUES('c.char','Christian','Charenton');
INSERT INTO Client VALUES('j.lebailly','Jeanne', 'Le Bailly');
INSERT INTO Client VALUES('s.efendi', 'Sarah', 'Efendioglu');
INSERT INTO Client VALUES('j.vives', 'Jean', 'Vives');
INSERT INTO Client VALUES('c.roth', 'Clémence', 'Roth');

INSERT INTO Occasionnel VALUES('p.dupont');
INSERT INTO Occasionnel VALUES('c.char');
INSERT INTO Occasionnel VALUES('j.lebailly');
INSERT INTO Occasionnel VALUES('s.efendi');

INSERT INTO Abonne VALUES('l.colt');
INSERT INTO Abonne VALUES('s.jahar');
INSERT INTO Abonne VALUES('j.vives');
INSERT INTO Abonne VALUES('c.roth');

INSERT INTO Compte_Fidelite VALUES(1, '2023-10-02', 110, 'bronze', 'l.colt');
INSERT INTO Compte_Fidelite VALUES(2, '2021-03-01', 450, 'argent', 's.jahar');
INSERT INTO Compte_Fidelite VALUES(3, '2022-11-10', 130, 'bronze', 'j.vives');
INSERT INTO Compte_Fidelite VALUES(4, '2023-07-15', 540, 'or', 'c.roth');

INSERT INTO Zones VALUES(1,'Rouge', 8.0,  320.0);
INSERT INTO Zones VALUES(2,'Orange', 6.0,  280.0);
INSERT INTO Zones VALUES(3,'Jaune', 4.0, 220.0 );
INSERT INTO Zones VALUES(4,'Vert', 2.0,  180.0);

INSERT INTO Carte_Abonnement VALUES (1, '2024-08-02','2024-09-02',1, 'l.colt',2, '2024-08-01','carte', 4);
INSERT INTO Carte_Abonnement VALUES (2, '2024-06-10','2024-07-10',2, 's.jahar',4, '2024-06-02','guichet', 2);
INSERT INTO Carte_Abonnement VALUES (3, '2024-10-02','2024-11-20',3, 'j.vives',6, '2024-12-20','carte', 1);
INSERT INTO Carte_Abonnement VALUES (4, '2024-10-02','2024-05-03',4, 'c.roth',8, '2024-12-20','carte', 1);

INSERT INTO Parking VALUES (1, 'David Bo-oui', '30 avenue du Tertre', 150, 1);
INSERT INTO Parking VALUES (2, 'UTCool', '3 place Benjamin Franklin', 50, 3);
INSERT INTO Parking VALUES (3, 'Chaquira', '271 impasse du Lolelole', 200, 1);
INSERT INTO Parking VALUES (4, 'Rattata', '25 rue David Lafarge', 150, 1);
INSERT INTO Parking VALUES (5,'Magick Sistem','24 rue du moulin',269,1);
INSERT INTO Parking VALUES (6,'Mpokorat','12 avenue du pendu',180,2);
INSERT INTO Parking VALUES (7,'Dalmatiens','41 chemin de Cruella',101,3);
INSERT INTO Parking VALUES (8,'Colonel raiyel','25 chemin de celui',70,4);

INSERT INTO Place VALUES (1, '1', 'occupee', 'deux_roues', 'couverte', 1);
INSERT INTO Place VALUES (2, '2', 'libre', 'camion', 'couverte', 1);
INSERT INTO Place VALUES (3, '3', 'reservee', 'deux_roues', 'couverte', 1);
INSERT INTO Place VALUES (4, '1', 'libre', 'voiture', 'plein_air', 2);
INSERT INTO Place VALUES (5, '2', 'libre', 'voiture', 'plein_air', 2);
INSERT INTO Place VALUES (6, '3', 'occupee', 'deux_roues', 'plein_air', 2);
INSERT INTO Place VALUES (7, '1', 'occupee', 'deux_roues', 'plein_air', 3);
INSERT INTO Place VALUES (8, '2', 'occupee', 'voiture', 'couverte', 3);
INSERT INTO Place VALUES (9, '3', 'occupee', 'camion', 'couverte', 3);
INSERT INTO Place VALUES (10, '1', 'occupee', 'deux_roues', 'couverte', 4);
INSERT INTO Place VALUES (11, '2', 'occupee', 'camion', 'couverte', 4);
INSERT INTO Place VALUES (12, '3', 'occupee', 'camion', 'couverte', 4);
INSERT INTO Place VALUES (13,'1','libre','camion','couverte',5);
INSERT INTO Place VALUES (14,'2','occupee','camion','couverte',5);
INSERT INTO Place VALUES (15,'3','occupee','camion','couverte',5);
INSERT INTO Place VALUES (16,'1','libre','deux_roues','couverte',6);
INSERT INTO Place VALUES (17,'2','reservee','deux_roues','couverte',6);
INSERT INTO Place VALUES (18,'3','libre','deux_roues','couverte',6);
INSERT INTO Place VALUES (19,'1','libre','voiture','plein_air',7);
INSERT INTO Place VALUES (20,'2','libre','voiture','plein_air',7);
INSERT INTO Place VALUES (21,'3','libre','camion','plein_air',7);
INSERT INTO Place VALUES (22,'1','occupee','voiture','couverte',8);
INSERT INTO Place VALUES (23,'2','occupee','voiture','plein_air',8);
INSERT INTO Place VALUES (24,'3','occupee','voiture','plein_air',8);

INSERT INTO Ticket VALUES (1,'10:34:53','12:34:53','2024-11-04','carte',2);
INSERT INTO Ticket VALUES (2,'14:10:00','18:10:00','2024-11-03','guichet',3);
INSERT INTO Ticket VALUES (3,'08:20:00','19:20:00','2024-10-16','guichet',1);
INSERT INTO Ticket VALUES (4,'15:00:00','17:00:00','2024-11-01','carte',4);
INSERT INTO Ticket VALUES (5,'11:30:00','15:30:00','2024-09-14','carte',2);
INSERT INTO Ticket VALUES (6,'17:10:00','21:10:00','2024-10-29','guichet',3);
INSERT INTO Ticket VALUES (7,'10:20:00','18:20:00','2024-08-16','guichet',3);
INSERT INTO Ticket VALUES (8,'14:30:00','17:30:00','2024-10-01','carte',1);
INSERT INTO Ticket VALUES (9,'12:30:00','13:30:00','2024-12-01','carte',1);

INSERT INTO Vehicule VALUES ('PL-123-AK', 'voiture', 1,'p.dupont');
INSERT INTO Vehicule VALUES ('MK-158-JF', 'voiture',2,'l.colt');
INSERT INTO Vehicule VALUES ('AZ-562-SL', 'camion',5,'s.jahar');
INSERT INTO Vehicule VALUES ('JI-987-KO', 'deux_roues',8,'p.dupont');
INSERT INTO Vehicule VALUES ('GT-578-OI', 'camion',7,'c.char');
INSERT INTO Vehicule VALUES ('PU-548-YT', 'camion',3,'j.lebailly');
INSERT INTO Vehicule VALUES ('FR-943-DH', 'voiture',4,'s.efendi');
INSERT INTO Vehicule VALUES ('XS-524-VC', 'voiture',6,'j.vives');
INSERT INTO Vehicule VALUES ('OL-094-IC', 'deux_roues',9,'c.roth');

INSERT INTO Reservation VALUES ('c.roth',3,1,'2024-10-02','2024-10-03');
INSERT INTO Reservation VALUES ('j.vives',17,2,'2024-11-22','2024-11-23');



----------- gerer tarif en fonction abonnement



-- Mise à jour du niveau de fidélité pour tous les abonnés basé sur les nouveaux points
UPDATE Compte_Fidelite
SET niveau = CASE
                WHEN points > 1000 THEN 'or'
                WHEN points > 400 THEN 'argent'
                WHEN points > 150 THEN 'bronze'
             END;

-- Mise à jour des points de fidélité pour tous les abonnés après une nouvelle réservation
UPDATE Compte_Fidelite
SET points = points + 10
WHERE login_abonne IN (
    SELECT DISTINCT login_client
    FROM Reservation
);

-- Mise à jour des points de fidélité pour tous les abonnés après un nouvel abonnement
UPDATE Compte_Fidelite
SET points = points + 100
WHERE id_fidelite IN (
    SELECT DISTINCT compte_fidelite
    FROM Carte_Abonnement
);

-- Mise à jour du niveau de fidélité pour tous les abonnés basé sur les nouveaux points
UPDATE Compte_Fidelite
SET niveau = CASE
                WHEN points > 1000 THEN 'or'
                WHEN points > 400 THEN 'argent'
                WHEN points > 150 THEN 'bronze'
             END;

-- Application des réductions en fonction des niveaux de fidélité
-- Réduction de tarif pour les abonnés de niveau 'bronze' (10%)
UPDATE Zones
SET tarif_abo = tarif_abo * 0.90
WHERE id_zone IN (
    SELECT DISTINCT zone
    FROM Carte_Abonnement ca
    JOIN Compte_Fidelite cf ON ca.compte_fidelite = cf.id_fidelite
    WHERE cf.niveau = 'bronze'
);

-- Réduction de tarif pour les abonnés de niveau 'argent' (15%)
UPDATE Zones
SET tarif_abo = tarif_abo * 0.85
WHERE id_zone IN (
    SELECT DISTINCT zone
    FROM Carte_Abonnement ca
    JOIN Compte_Fidelite cf ON ca.compte_fidelite = cf.id_fidelite
    WHERE cf.niveau = 'argent'
);

-- Réduction de tarif pour les abonnés de niveau 'or' (25%)
UPDATE Zones
SET tarif_abo = tarif_abo * 0.75
WHERE id_zone IN (
    SELECT DISTINCT zone
    FROM Carte_Abonnement ca
    JOIN Compte_Fidelite cf ON ca.compte_fidelite = cf.id_fidelite
    WHERE cf.niveau = 'or'
);

-- Requête pour gérer les infos des clients
SELECT * FROM Client;


-------------------- afficher des nombres de trucs 


-- Requête pour connaître le nombre de clients (+ nombre d’abonnés et nombre d’occasionnels)
SELECT 
    (SELECT COUNT(*) FROM Client) AS nb_total_clients,
    (SELECT COUNT(*) FROM Abonne) AS nb_total_abonnes,
    (SELECT COUNT(*) FROM Occasionnel) AS nb_total_occasionnels;

-- Requête pour connaître le nombre de clients/parking (+ nombre d’abonnés et nombre d’occasionnels)
SELECT 
    pa.nom AS nom_parking,
    COUNT(DISTINCT cl.login_client) AS nb_clients_parking,
    COUNT(DISTINCT ab.login_abonne) AS nb_abonnes_parking,
    COUNT(DISTINCT oc.login_occasionnel) AS nb_occasionnels_parking
FROM Parking pa
JOIN Place pl ON pl.parking = pa.id_parking
JOIN Reservation res ON res.place_reservee = pl.id_place
JOIN Client cl ON cl.login_client = res.login_client
LEFT JOIN Abonne ab ON cl.login_client = ab.login_abonne
LEFT JOIN Occasionnel oc ON cl.login_client = oc.login_occasionnel
GROUP BY pa.nom;

-- Requête pour connaître le nombre de clients/zone (+ nombre d’abonnés et nombre d’occasionnels)
SELECT 
    z.nom AS nom_zone,
    COUNT(DISTINCT cl.login_client) AS nb_clients_zone,
    COUNT(DISTINCT ab.login_abonne) AS nb_abonnes_zone,
    COUNT(DISTINCT oc.login_occasionnel) AS nb_occasionnels_zone
FROM Zones z
JOIN Parking pa ON pa.zone_parking = z.id_zone
JOIN Place pl ON pl.parking = pa.id_parking
JOIN Reservation res ON res.place_reservee = pl.id_place
JOIN Client cl ON cl.login_client = res.login_client
LEFT JOIN Abonne ab ON cl.login_client = ab.login_abonne
LEFT JOIN Occasionnel oc ON cl.login_client = oc.login_occasionnel 
GROUP BY z.nom;

-- Requête pour afficher les réservations par client
SELECT c.nom, c.prenom, pl.numero AS num_place, pa.nom AS nom_parking, r.date_debut, r.date_fin
FROM Reservation r
JOIN Client c ON r.login_client = c.login_client
JOIN Place pl ON r.place_reservee = pl.id_place
JOIN Parking pa ON pl.parking = pa.id_parking;

-- Requête pour lister les comptes de fidélité, leurs points et leurs niveaux
SELECT cf.id_fidelite, c.nom, c.prenom, cf.points, cf.niveau
FROM Compte_Fidelite cf
JOIN Abonne a ON cf.login_abonne = a.login_abonne
JOIN Client c ON a.login_abonne = c.login_client;

-- Requête pour compter le nombre de places disponibles par parking
SELECT pa.nom AS nom_parking, COUNT(pl.id_place) AS places_dispo
FROM Place pl
JOIN Parking pa ON pl.parking = pa.id_parking
WHERE pl.statut = 'libre'
GROUP BY pa.nom;


-------------------- mettre à jour des tarif  et zone parking


-- Requête pour mettre à jour le tarif horaire d'une zone spécifique
UPDATE Zones
SET tarif_horaire = 7.50, tarif_abo = 300.00
WHERE id_zone = 1;

-- Requête pour changer la zone d'un parking
UPDATE Parking
SET zone_parking = 2
WHERE id_parking = 1;



----------- statistiques 


-- Étude Statistique sur les Tarifs par Zone
SELECT 
    z.nom AS nom_zone,
    AVG(z.tarif_horaire) AS tarif_moyen_horaire,
    MIN(z.tarif_horaire) AS tarif_minimum_horaire,
    MAX(z.tarif_horaire) AS tarif_maximum_horaire
FROM Zones z
GROUP BY z.nom;

-- Étude Statistique sur l'Occupation des Parkings
-- Afficher le nombre total de places, la moyenne des places libres, ainsi que les valeurs min et max pour chaque parking
SELECT 
    pa.nom AS nom_parking,
    COUNT(pl.id_place) AS nb_total_places
FROM Place pl
JOIN Parking pa ON pl.parking = pa.id_parking
GROUP BY pa.nom;

SELECT 
    pa.nom AS nom_parking,
    COUNT(pl.id_place) AS nb_places_dispo
FROM Place pl
JOIN Parking pa ON pl.parking = pa.id_parking
WHERE pl.statut = 'libre'
GROUP BY pa.nom;

SELECT 
    pa.nom AS nom_parking,
    COUNT(pl.id_place) AS nb_places_occupees
FROM Place pl
JOIN Parking pa ON pl.parking = pa.id_parking
WHERE pl.statut = 'occupee'
GROUP BY pa.nom;

SELECT 
    AVG(nb_places_dispo) AS moyenne_places_dispo
FROM (
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_places_dispo
    FROM Place pl
    JOIN Parking pa ON pl.parking = pa.id_parking
    WHERE pl.statut = 'libre'
    GROUP BY pa.nom
) AS places_dispo;

SELECT 
    MIN(nb_places_dispo) AS min_places_dispo
FROM (
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_places_dispo
    FROM Place pl
    JOIN Parking pa ON pl.parking = pa.id_parking
    WHERE pl.statut = 'libre'
    GROUP BY pa.nom
) AS places_dispo;

SELECT 
    MAX(nb_places_dispo) AS max_places_dispo
FROM (
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_places_dispo
    FROM Place pl
    JOIN Parking pa ON pl.parking = pa.id_parking
    WHERE pl.statut = 'libre'
    GROUP BY pa.nom
) AS places_dispo;



-- Étude Statistique sur les Points de Fidélité
SELECT 
    cf.niveau,
    AVG(cf.points) AS points_moyens,
    MIN(cf.points) AS points_minimum,
    MAX(cf.points) AS points_maximum
FROM Compte_Fidelite cf
GROUP BY cf.niveau;

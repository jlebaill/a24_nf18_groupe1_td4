import psycopg2

# Fonction pour afficher les statistiques des parkings
def afficher_stats_parking(cursor):
    query = """
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_total_places,
        SUM(CASE WHEN pl.statut = 'libre' THEN 1 ELSE 0 END) AS nb_places_libres,
        SUM(CASE WHEN pl.statut = 'occupee' THEN 1 ELSE 0 END) AS nb_places_occupees
    FROM Parking pa
    JOIN Place pl ON pl.parking = pa.id_parking
    GROUP BY pa.nom;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Statistiques des Parkings ===")
    for row in results:
        print(f"Parking {row[0]} : Total {row[1]} places, {row[2]} libres, {row[3]} occupées")

import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()


# Fonction pour afficher le nombre de clients (total, abonnés, occasionnels)
def afficher_stats_clients(cursor):
    query = """
    SELECT 
        (SELECT COUNT(*) FROM Client) AS nb_total_clients,
        (SELECT COUNT(*) FROM Abonne) AS nb_total_abonnes,
        (SELECT COUNT(*) FROM Occasionnel) AS nb_total_occasionnels;
    """
    cursor.execute(query)
    result = cursor.fetchone()
    print("=== Statistiques sur les Clients ===")
    print(f"Nombre total de clients : {result[0]}")
    print(f"Nombre d'abonnés : {result[1]}")
    print(f"Nombre d'occasionnels : {result[2]}")


import psycopg2
from client.py import *
from parking.py import *
from transaction.py import *
from vehicule.py import *


# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()




# Fonction principale du menu
def menu():

    login_client = input("Entrez votre login pour vous connecter (vide si nouveau client): ")
    if login_client == None :
        login_client = creer_compte()
    if not verifier_login(login_client):
        login_client = creer_compte()

    
    choix = 0
    while choix != 7:
        print("\n ======================================")
        print("\n 1. Créer un compte client")
        print("\n 2. Devenir abonné")
        print("\n 3. Réserver une place de parking")
        print("\n 4. Enregistrer un de ses véhicules")
        print("\n 5. Visualiser mon Espace Client")
        print("\n 6. Acheter un ticket")
        print("\n 7. Déconnexion")
        print("\n ======================================")

        try:
            choix = int(input("Saisissez votre choix : "))
            match choix:
                case 1:
                    creer_compte()
                case 2:
                    devenir_abonne(login_client)
                case 3:
                    reserver_place(login_client)
                case 4:
                    enregistrer_vehicule(login_client)
                case 5:
                    visualiser_espace_client(login_client)
                case 6:
                    acheter_ticket()
                case 7:
                    print("\n======== PROGRAMME TERMINE ========\n")
                case _:
                    print("\n\nERREUR : votre choix n'est pas valide ! ")
        except ValueError:
            print("Erreur : veuillez entrer un nombre.")

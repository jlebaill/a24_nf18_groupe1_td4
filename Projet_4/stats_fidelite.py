import psycopg2

# Fonction pour afficher les statistiques des points de fidélité
def afficher_stats_points_fidelite(cursor):
    query = """
    SELECT 
        cf.niveau,
        AVG(cf.points) AS points_moyens,
        MIN(cf.points) AS points_minimum,
        MAX(cf.points) AS points_maximum
    FROM Compte_Fidelite cf
    GROUP BY cf.niveau;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Statistiques des Points de Fidélité ===")
    for row in results:
        print(f"Niveau {row[0]} : Moyenne = {row[1]:.2f}, Min = {row[2]}, Max = {row[3]}")

# Fonction pour afficher les comptes de fidélité et leurs niveaux
def afficher_comptes_fidelite(cursor):
    query = """
    SELECT cf.id_fidelite, c.nom, c.prenom, cf.points, cf.niveau
    FROM Compte_Fidelite cf
    JOIN Abonne a ON cf.login_abonne = a.login_abonne
    JOIN Client c ON a.login_abonne = c.login_client;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Liste des Comptes de Fidélité ===")
    for row in results:
        print(f"Fidélité ID {row[0]} : {row[1]} {row[2]}, Points = {row[3]}, Niveau = {row[4]}")

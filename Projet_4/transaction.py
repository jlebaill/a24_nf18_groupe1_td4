import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

# Fonction 1: Acheter un ticket
def acheter_ticket(login_client):
    date = input("Entrez la date (YYYY-MM-DD) : ")
    debut = input("Entrez l'heure de début (HH:MM:SS) : ")
    fin = input("Entrez l'heure de fin (HH:MM:SS) : ")
    
    moyen_paiement = input("Entrez le moyen de paiement (carte, guichet) : ")

    # On lui affiche la liste de ses véhicules pour lequel il souhaite acheter un ticket et on lui demande d'en choisir un
    cur.execute("SELECT immatriculation FROM Vehicule WHERE login_client = %s",(login_client,))
    rows = cur.fetchall() 
    # Afficher les résultats
    for row in rows:
        print(f"{row[0]}")
    
    vehicule = input("Veuillez écrire en toute lettre la plaque d'immatriculation de la voiture concerné :")


    # On lui affiche la liste des zones pour qu'il choisisse où souhaite se garer
    cur.execute("SELECT id_zone, nom FROM Zones")
    rows = cur.fetchall() 
    # Afficher les résultats
    for row in rows:
        print(f"{row[0]}.{row[1]}")

    zone = input("Entrez l'ID de la zone : ")
    
    # On récupère l'id qu'on incrémente
    cur.execute("SELECT MAX(id_ticket) FROM Ticket")
    key = cur.fetchone()[0] + 1
    
    try:
        cur.execute("INSERT INTO Ticket (id_ticket, debut, fin, date, moyen_paiement, zone) VALUES (%s,%s, %s, %s, %s, %s)",
                    (key,debut, fin, date, moyen_paiement, zone))
        conn.commit()
        print("Ticket acheté avec succès.")
    except Exception as e:
        conn.rollback()
        print(f"Erreur lors de l'achat du ticket : {e}")

    try:
        cur.execute("UPDATE Vehicule SET ticket_vehicule = %s WHERE immatriculation = %s ",
                    (key,vehicule))
        conn.commit()
        print("Succés de l'update dans la table véhicule.")
    except Exception as e:
        conn.rollback()
        print(f"Erreur lors de l'update dans la table véhicule. : {e}")

    

    conn.close()
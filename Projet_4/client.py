
import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()



def verifier_login(login_client):
    resultat = []
    cur.execute("SELECT prenom,nom FROM Client WHERE login_client = %s", (login_client,))
    if cur.fetchone() is None:
        print("Vous n'avez pas de compte veuillez en créer un.")
        return False
    else:
        resultat= cur.fetchone()
        print(resultat)
        return True



# Fonction 1: Créer un compte client
def creer_compte():
    creer = False
    while creer == False:
    	
        nom = input("Entrez votre nom :")
        prenom = input("Entrez votre prénom :")
        login = input("Veuillez créer votre login :")
 	
        try:
            cur.execute("INSERT INTO Client (login_client, nom, prenom) VALUES (%s, %s, %s)", (login, nom, prenom))
            conn.commit()
            print("Compte client créé avec succès.")
            creer = True
        except psycopg2.IntegrityError:
            conn.rollback()
            print("Erreur : Ce login est déjà utilisé.")
    return login

# Fonction 3: Visualiser l'espace client
def visualiser_espace_client(login_client):
    cur.execute("SELECT * FROM Client WHERE login_client = %s", (login_client,))
    client_info = cur.fetchone()
    if client_info:
        print("\n===== Informations Client =====")
        print(f"Login : {client_info[0]}")
        print(f"Nom : {client_info[1]}")
        print(f"Prénom : {client_info[2]}")
        # Affichez des informations supplémentaires ici si nécessaire (abonnements, réservations, etc.)
    else:
        print("Aucune information trouvée pour ce client.")


def devenir_abonne(login_client):
    try:
        

        # Créer un compte fidélité
        print("Création d'un compte fidélité...")
        date_creation = date.today()
        points = 0
        niveau = 'bronze'

        cur.execute("""
            INSERT INTO Compte_Fidelite (date_creation, points, niveau, login_abonne)
            VALUES (%s, %s, %s, %s) RETURNING id_fidelite;
        """, (date_creation, points, niveau, login_client))
        id_fidelite = cur.fetchone()[0]
        print(f"Compte fidélité créé avec succès. ID : {id_fidelite}")

        # Proposer les zones avec tarifs ajustés
        print("Voici les zones disponibles avec les tarifs ajustés selon votre niveau de fidélité :")

        cur.execute("""
            SELECT z.id_zone, z.nom, z.tarif_abo, cf.niveau
            FROM Zones z
            CROSS JOIN (SELECT niveau FROM Compte_Fidelite WHERE login_abonne = %s) cf;
        """, (login_client,))
        zones = cur.fetchall()

        for zone in zones:
            id_zone, nom, tarif_base, niveau = zone
            reduction = 1.0  # Par défaut, pas de réduction
            if niveau == 'argent':
                reduction = 0.85
            elif niveau == 'or':
                reduction = 0.75
            elif niveau == 'bronze':
                reduction = 0.95

            tarif_reduit = tarif_base * reduction
            print(f"ID Zone : {id_zone} | Nom : {nom} | Tarif avec réduction ({niveau}): {tarif_reduit:.2f} €")

        # Choisir une zone
        zone_id = int(input("Choisissez une zone (ID Zone) : "))

        # Vérifier si la zone existe
        cur.execute("SELECT * FROM Zones WHERE id_zone = %s;", (zone_id,))
        selected_zone = cur.fetchone()
        if not selected_zone:
            print("Erreur : Zone non valide.")
            return

        # Créer une carte d'abonnement
        print("Création de la carte d'abonnement...")
        date_de_debut = date.today()
        date_de_fin = date(date_de_debut.year + 1, date_de_debut.month, date_de_debut.day)  # 1 an
        moyen_paiement = input("Entrez le moyen de paiement (carte/guichet) : ")

        cur.execute("""
            INSERT INTO Carte_Abonnement (date_de_debut, date_de_fin, compte_fidelite, login_abonne, date_transaction, moyen_paiement, zone)
            VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING id_abonnement;
        """, (date_de_debut, date_de_fin, id_fidelite, login_client, date_de_debut, moyen_paiement, zone_id))
        id_abonnement = cur.fetchone()[0]

        print(f"Carte d'abonnement créée avec succès. ID : {id_abonnement}")

        # Sauvegarder les changements
        conn.commit()

    except Exception as e:
        print("Une erreur s'est produite :", e)
        if conn:
            conn.rollback()

    finally:
        if conn:
            cur.close()
            conn.close()

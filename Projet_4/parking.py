import psycopg2

# Connexion à la base de données
conn = psycopg2.connect("host='tuxa.sme.utc' dbname='dbnf18a097' user='nf18a097' password='BhC0jpjW7Bmk'")
cur = conn.cursor()

# Fonction 1: Réserver une place de parking
def reserver_place(login_client):

    date_debut = input("Entrez la date de début (YYYY-MM-DD) : ")
    date_fin = input("Entrez la date de fin (YYYY-MM-DD) : ")
    
    # On lui montre les différents parkings possibles
    cur.execute("SELECT id_parking, nom FROM Parking")
    rows = cur.fetchall()  # Utilise 'cur' ici, car c'est le curseur que tu utilises pour exécuter la requête
    # Afficher les résultats
    for row in rows:
        print(f"{row[0]}.{row[1]}")

    parking = input("Entrez l'ID du parking souhaitée : ")
    
    # On lui montre les différentes places disponibles
    cur.execute("SELECT id_place, type_de_vehicule FROM Place WHERE parking = %s",(parking))
    rows = cur.fetchall()  # Utilise 'cur' ici, car c'est le curseur que tu utilises pour exécuter la requête
    # Afficher les résultats
    for row in rows:
        print(f"{row[0]}.{row[1]}")

    id_place = input("Entrez l'ID de la place souhaitée en fonction du type de votre véhicule : ")
    
    # On récupère l'id qu'on incrémente
    cur.execute("SELECT MAX(id_reservation) FROM Reservation")
    key = cur.fetchone()[0] + 1

    try:
        cur.execute("INSERT INTO Reservation (login_client, place_reservee,id_reservation, date_debut, date_fin) VALUES (%s, %s, %s, %s, %s)",
                    (login_client, id_place, key, date_debut, date_fin))
        conn.commit()
        print("Réservation effectuée avec succès.")
    except Exception as e:
        conn.rollback()
        print(f"Erreur lors de la réservation : {e}")
import psycopg2

# Fonction pour afficher les statistiques des places par parking
def afficher_stats_places_parking(cursor):
    query = """
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_total_places,
        SUM(CASE WHEN pl.statut = 'libre' THEN 1 ELSE 0 END) AS nb_places_libres,
        SUM(CASE WHEN pl.statut = 'occupee' THEN 1 ELSE 0 END) AS nb_places_occupees
    FROM Parking pa
    JOIN Place pl ON pl.parking = pa.id_parking
    GROUP BY pa.nom;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Statistiques des Places par Parking ===")
    for row in results:
        print(f"Parking {row[0]} : Total = {row[1]}, Libres = {row[2]}, Occupées = {row[3]}")

# Fonction pour afficher les moyennes, min et max des places libres par parking
def afficher_stats_places_libres(cursor):
    query = """
    SELECT 
        pa.nom AS nom_parking,
        COUNT(pl.id_place) AS nb_places_libres
    FROM Place pl
    JOIN Parking pa ON pl.parking = pa.id_parking
    WHERE pl.statut = 'libre'
    GROUP BY pa.nom;
    """
    cursor.execute(query)
    results = cursor.fetchall()
    print("=== Places Libres par Parking ===")
    for row in results:
        print(f"Parking {row[0]} : {row[1]} places libres")
